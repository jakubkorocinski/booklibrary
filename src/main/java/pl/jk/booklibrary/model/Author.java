package pl.jk.booklibrary.model;

import java.time.LocalDate;

public class Author {
	
	private String firstName;
	private String secondName;
	private String surname;
	private LocalDate birthDate;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	
	public String getSecondName() {
		return this.secondName;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	
	public LocalDate getBirthDate() {
		return this.birthDate;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( !(o instanceof Author)) return false;
		Author author = (Author)o;
		return (this.getFirstName().equalsIgnoreCase(author.getFirstName()) && 
				this.getSurname().equalsIgnoreCase(author.getSurname()) &&
				this.getSecondName().equalsIgnoreCase(author.getSecondName()) &&
				this.getBirthDate().isEqual(author.getBirthDate())
				) ? true : false;
	}
	
	@Override
	public int hashCode() {
		return firstName.hashCode() + 7 * secondName.hashCode() + 7 * surname.hashCode() + 7 * birthDate.hashCode();
	}
	
	@Override
	public String toString() {
		return "[firstName=" + firstName + ", secondName=" 
				+ secondName + ", surname=" 
				+ surname + ", birthDay=" 
				+ birthDate.toString() + "]";
	}
	
	
	
}
